﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace IdleTimePublish
{
	internal class TcpServer
	{
		public TcpListener Listener { get => this.listener; }
		public bool Accept { get; set; } = true;
		private TcpListener listener;
		private Action<TcpClient> callback;

		public TcpServer(IPAddress address, int port, Action<TcpClient> client_callback)
		{
			this.listener = new TcpListener(address, port);
			this.callback = client_callback;
			listener.Start();
			this.listenAsync();
		}

		private async void listenAsync()
		{
			while (this.Accept)
			{
				TcpClient client = await this.listener.AcceptTcpClientAsync();
				Task.Run(() =>
				{
					this.callback.Invoke(client);
					client.GetStream().Dispose();
				});
			}
		}
	}
}
