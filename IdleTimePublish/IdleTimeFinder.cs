﻿using System;
using System.Runtime.InteropServices;

namespace IdleTimePublish
{
	// <https://stackoverflow.com/a/11901730/522479>
	internal struct LASTINPUTINFO
	{
		public uint cbSize;
		public uint dwTime;
	}

	internal class IdleTimeFinder
	{
		[DllImport("User32.dll")]
		private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

		[DllImport("Kernel32.dll")]
		private static extern uint GetLastError();

		public static uint GetIdleTime()
		{
			var lii = new LASTINPUTINFO();
			lii.cbSize = (uint)Marshal.SizeOf(lii);
			GetLastInputInfo(ref lii);

			return ((uint)Environment.TickCount - lii.dwTime);
		}

		public static long GetLastInputTime()
		{
			var lii = new LASTINPUTINFO();
			lii.cbSize = (uint)Marshal.SizeOf(lii);
			if (!GetLastInputInfo(ref lii))
				throw new Exception(GetLastError().ToString());
			return lii.dwTime;
		}
	}
}
