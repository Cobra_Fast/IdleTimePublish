﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace IdleTimePublish
{
	public class IdleTimePublish
	{
		private TcpServer ts;

		public void Run()
		{
			ts = new TcpServer(IPAddress.Any, 34543, (c) => {
				var IdleTimeRaw = IdleTimeFinder.GetIdleTime();
				var IdleTime = TimeSpan.FromMilliseconds(IdleTimeRaw);
				byte[] data = Encoding.ASCII.GetBytes(((int)IdleTime.TotalSeconds).ToString() + "\n");
				c.GetStream().Write(data, 0, data.Length);
			});
		}

		public void Stop()
		{
			ts.Accept = false;
		}
	}
}
