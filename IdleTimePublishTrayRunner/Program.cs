﻿using System;
using System.Windows;
using System.Drawing;

using Hardcodet.Wpf.TaskbarNotification;
using IdleTimePublish;

namespace IdleTimePublishTrayRunner
{
	class Program
	{
		[STAThread]
		public static void Main(string[] Args)
		{
			var app = new Application();
			app.Startup += App_Startup;
			app.Run();
		}

		private static void App_Startup(object sender, StartupEventArgs e)
		{
			TaskbarIcon tbi = new TaskbarIcon();
			tbi.Icon = Icon.FromHandle(Resource.clock_network.GetHicon());
			tbi.ToolTipText = @"Idle Time Publish";

			var itp = new IdleTimePublish.IdleTimePublish();
			itp.Run();
		}	
	}
}
